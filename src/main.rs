//! spfl
//! Experiments in procedural audio with Rust
//! Josh Berson, April 2021

use std::sync::mpsc::{Receiver, TryRecvError};

// Audio host
use cpal;
use cpal::traits::{DeviceTrait, HostTrait, StreamTrait};

use nannou::prelude::*;

mod flux;
use flux::FluxMethod::*;

fn main() {
    nannou::app(model)
        .update(update)
        .view(view)
        .run();
}

struct Model {
    _chan: usize,
    _adc_stream: cpal::Stream,
    flux1_rx: Receiver<[f32; flux::BINS]>,
    flux2_rx: Receiver<[f32; flux::BINS]>,
    flux1_dis: [f32; flux::BINS],
    flux2_dis: [f32; flux::BINS],
}

impl Model {
    pub fn flux_recv(&mut self) {
        match self.flux1_rx.try_recv() {
            Ok(dis) => self.flux1_dis = dis,
            Err(TryRecvError::Empty) => (),
            Err(TryRecvError::Disconnected) => 
                println!("Flux report channel disconnected"),
        };
        match self.flux2_rx.try_recv() {
            Ok(dis) => self.flux2_dis = dis,
            Err(TryRecvError::Empty) => (),
            Err(TryRecvError::Disconnected) => 
                println!("Flux report channel disconnected"),
        };
    }

    pub fn flux(&self) -> (&[f32], &[f32]) {
        (&self.flux1_dis, &self.flux2_dis)
    }
}

fn model(app: &App) -> Model {
    app.new_window()
        .size(1280, 720)
        .build()
        .unwrap();

    // Connect to the adc: https://docs.rs/cpal/
    // FUTURE: Enumerate devices with host.devices()

    let host = cpal::default_host();
    let adc = host.default_input_device()
        .expect("No input devices found");
    let mut adc_configs = adc.supported_input_configs()
        .expect("Error querying input configurations");
    let adc_config = adc_configs.next()
        .expect("No supported input configurations")
        .with_max_sample_rate();

    // Instantiate flux device along with an mpsc::Receiver to receive its reports
    let chan: usize = adc_config.channels() as usize;
    let (mut flux1, flux1_rx) = flux::Flux::new(adc_config.sample_rate().0 as usize, chan, 0);
    let (mut flux2, flux2_rx) = flux::Flux::new(adc_config.sample_rate().0 as usize, chan, 0);
    flux1.set_window(30.0);
    flux2.set_window(30.0);
    //flux2.set_clip(0.006);
    flux2.set_method(Onset);

    // Set up an adc stream, hand it the flux device
    let adc_stream = adc.build_input_stream(
        &adc_config.into(),
        move |samples: &[f32], _: &cpal::InputCallbackInfo| {
            for s in samples.iter() {
                flux1.push(*s);
                flux2.push(*s);
            }
        },
        move |err| {
            println!("Error reading from adc: {}", err);
        }
    ).unwrap();
    adc_stream.play().unwrap();

    // No need to detach the streams in separate threads:
    // https://docs.rs/cpal/0.13.2/cpal/index.html

    Model {
        _chan: chan,
        _adc_stream: adc_stream,
        flux1_rx,
        flux2_rx,
        flux1_dis: [0.0; flux::BINS],
        flux2_dis: [0.0; flux::BINS],
    }
}

//
// Event loop

fn update(_app: &App, model: &mut Model, _update: Update) {
    model.flux_recv();
}

fn view(app: &App, model: &Model, frame: Frame) {
    let (flux1, flux2) = model.flux();
    let bins: f32 = flux1.len() as f32;
    let xscale = 600.0f32 / bins;
    let yscale = 400.0f32 * xscale;
    let dis1 = (0..flux1.len()).map(|i| {
        let x = xscale * ((i as f32) - bins/2.0);
        let y = yscale * (flux1[i] - 0.2);
        (pt2(x,y), GRAY)
    });
    let dis2 = (0..flux2.len()).map(|i| {
        let x = xscale * ((i as f32) - bins/2.0);
        let y = yscale * (flux2[i] - 0.2);
        (pt2(x,y), LIGHTGRAY)
    });

    frame.clear(WHITE);
    let draw = app.draw();
    draw.polyline().weight(3.0).points_colored(dis1);
    draw.polyline().weight(3.0).points_colored(dis2);
    draw.to_frame(app, &frame).unwrap();
}