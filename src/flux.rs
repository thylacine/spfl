// mod flux: Asynchronous single-channel spectral flux histogram

use std::cmp::min;
use std::f32::consts;
use decorum::cmp; // {min, max}_or_undefined() for floats
use transpose::transpose; // Vector-as-matrix: reshaping and transposition
use std::time::{Duration, Instant};

use std::sync::mpsc::{Sender, Receiver, channel};

use rustfft::{Fft, FftDirection, algorithm::Radix4, num_complex::Complex};

pub const FLUX_FRAME_MS: usize = 50; //100; // spectral flux frame in ms
pub const FFT_FRAME: usize = 1<<9; // fft frame in samples
pub const FFT_STEP: usize = 1<<8;

pub const FLUX_WINDOW_SEC: f32 = 60.0;
pub const PRUNE_IF_SEC: f32 = 120.0;

pub const BINS: usize = 200;
const CLIP: f32 = 0.003; // Try values in [0.001, 0.004]
	// https://www.sciencedirect.com/topics/engineering/spectral-flux
	// Giannakopoulos and Pikrakis define spectral flux as the
	// inner product of the delta between successive psd frames
	// and clip at 0.04 “mean” spectral flux — but “mean” formulated how?
	// Other versions of flux: 2-norm (sqrt of the inner product), RMS
	// Currently we’re implementing RMS

pub enum FluxMethod {
	Symmetric,
	Onset,
}

pub struct Flux {
	chan: usize,
	chan_i: usize,
	frame: usize,
	sample_buf: Vec<f32>,

	fft: Radix4<f32>,
	fft_buf: [Complex<f32>; FFT_FRAME],
	fft_tmp: [Complex<f32>; FFT_FRAME],

	method: FluxMethod,
	clip: f32,
	his: Vec<Vec<Instant>>,
	report: [f32; BINS],

	window: Duration,
	prune_if: Duration,
	pruned: Instant,

	tx: Sender<[f32; BINS]>,
}

impl Flux {
	// Takes: sample rate, # channels, which channel this device is for
	// Returns: (Flux, mpsc::Receiver to receive reports) 
	pub fn new(fs: usize, mut chan: usize, mut chan_i: usize) -> (Self, Receiver<[f32; BINS]>) {
		// Sanity
		if chan < 1 { chan = 1; }
		if chan_i >= chan { chan_i = 0; }

		let mut frame = fs * FLUX_FRAME_MS / 1000;
		frame += FFT_STEP - (frame % FFT_STEP);
			// Sample buffer must accommodate a whole number of FFT frames
			// (thus, FFT_STEP must be a divisor of FFT_FRAME)
		frame *= chan;
			// Channels are interleaved in the sample stream

		let sample_buf = Vec::<f32>::with_capacity(frame);

		let fft = Radix4::<f32>::new(FFT_FRAME, FftDirection::Forward);
		let fft_buf = [Complex{re:0.0f32, im:0.0f32}; FFT_FRAME];
		let fft_tmp = [Complex{re:0.0f32, im:0.0f32}; FFT_FRAME];

		let his = vec![Vec::<Instant>::new(); BINS];
		let report = [0.0f32; BINS];

		let window: Duration = Duration::from_secs_f32(FLUX_WINDOW_SEC);
		let prune_if: Duration = Duration::from_secs_f32(PRUNE_IF_SEC);
		let pruned = Instant::now();

		let (tx, rx) = channel::<[f32; BINS]>();

		(
			Flux {
				chan, chan_i, frame, sample_buf,
				fft, fft_buf, fft_tmp,
				clip: CLIP, method: FluxMethod::Symmetric,
				his, report,
				window, prune_if, pruned,
				tx,
			},
			rx
		)
	}

	pub fn set_window(&mut self, mut secs: f32) {
		if secs < 0.1 { secs = 0.1; }
		let window = Duration::from_secs_f32(secs);
		if self.prune_if <= window {
			self.prune_if = Duration::from_secs_f32(secs + 0.1);
		}
		self.window = window;
	}

	pub fn set_clip(&mut self, mut clip: f32) {
		if clip < 0.00001 { clip = 0.0001; }
		self.clip = clip;
	}

	pub fn set_method(&mut self, method: FluxMethod) {
		self.method = method;
	}

	pub fn push(&mut self, sample: f32) {
		// When the sample buffer is full, compute its mean spectral flux
		// and send a report down the channel
		if self.sample_buf.len() >= self.frame {
			// sample_buf should never get longer than frame, but just in case:
			// drop any excess frames (from the older end)
			let zero = self.sample_buf.len() - self.frame;
			self.sample_buf.drain(..zero);

			let chan_frame = self.frame / self.chan;

			// Samples arrive channel-interleaved
			// Transpose the contents of the buffer to get a row-major matrix by channel
			// Now each row contains the entire buffer for a single channel
			let mut deinterleaved = vec![0.0f32; self.frame];
			transpose(&self.sample_buf[..], &mut deinterleaved, self.chan, chan_frame);

			// Construct a vector of power spectra for the chan_ith channel
			// Each element represents the power spectrum for a sample vector of |FFT_FRAMES|
			let mut pspec_v = Vec::<Vec<f32>>::with_capacity(chan_frame / FFT_STEP - 1);
			let mut fft_p: usize = 0;
			while fft_p < chan_frame - FFT_STEP {
				let pspec = self.pspec(
					&deinterleaved[(fft_p + (self.chan_i * chan_frame))..(fft_p + FFT_FRAME + (self.chan_i * chan_frame))]);

				pspec_v.push(pspec);
				fft_p += FFT_STEP;
			}

			let now = Instant::now();

			// Periodically prune the histogram to prevent memory leak
			if now - self.pruned > self.prune_if {
				self.pruned = now;
				for bin in self.his.iter_mut() {
					// We want: bin.drain_filter(|ex| now - ex > self.window)
					// But drain_filter is not yet in stable: https://github.com/rust-lang/rust/issues/43244
					// Assumes examples appear in the bin in monotonically older-to-newer order
					let mut i = 0;
					while i < bin.len() {
						if now - bin[i] <= self.window { break }
						i += 1;
					}
					bin.drain(0..i);
				}
			}

			// Compute the flux between successive psd frames
			// and then the mean flux for all psd frames
			// For onsets, change fbin_b - fbin_a to [decorum::]cmp::max_or_undefined(fbin_b - fbin_a, 0.0)
			// (Really missing Julia’s dot vectorization)

			// In our 2019 Julia version this was:
			// 		delta = diff(psd; dims=2)
			//		flux = sqrt(sum(delta .^ 2)) / size(psd, 1)
			// That is, RMS rather than inner product or 2-norm

			let delta: Vec<Vec<f32>> = pspec_v.iter().zip(&pspec_v[1..])
				.map(|(frame_a, frame_b)| frame_a.iter().zip(frame_b)
					.map(|(fbin_a, fbin_b)|
						cmp::max_or_undefined(fbin_b - fbin_a,
							match self.method {
								FluxMethod::Symmetric => fbin_b - fbin_a,
								FluxMethod::Onset => 0.0,
							}))
							// Use fbin_b - fbin_a for symmetric flux,
							// max(fbin_b - fbin_a, 0.0) for onsets
					.collect())
				.collect();
			let flux_v: Vec<f32> = delta.iter()
				.map(|frame| frame.iter()
					.map(|fbin| fbin.powf(2.0)).collect::<Vec::<f32>>().iter()
						.fold(0.0f32, |sum: f32, fbin: &f32| sum + fbin)
							//.sqrt() // 2-norm. Comment out for inner product
						)
				.collect();
			let flux = flux_v.iter().fold(0.0f32, |sum, frame| sum + frame)
				.sqrt() // Use inner product version above and include this line for RMS
				/ flux_v.len() as f32;

			// Add the new example to the histogram
			let bin: usize = (BINS as f32 * (cmp::min_or_undefined(flux, self.clip) / self.clip)) as usize;
			self.his[min(bin, BINS - 1)].push(now);

			// Build the report (distribution)
			let mut total = 0.0f32;
			for (i, bin) in self.his.iter().enumerate() {
				self.report[i] = bin.iter()
					.fold(0.0, |sum, ex| sum +
						if now - *ex <= self.window { 1.0 }
						else { 0.0 });
				total += self.report[i];
			}
			// Normalize
			for bin in self.report.iter_mut() {
				*bin /= total;
			}

			self.tx.send(self.report).unwrap(); // Send the report

			// Shift the buffer forward in time by half its length
			self.sample_buf.drain(..(self.frame / 2));
		}
		self.sample_buf.push(sample); // Append the new sample
	}

	fn pspec(&mut self, frame: &[f32]) -> Vec<f32> {
		// Copy the frame into the fft buffer
		for f in self.fft_buf.iter_mut() {
			*f = Complex{re: 0.0f32, im: 0.0f32};
		}
		for (i, f) in frame.iter().enumerate() {
			if i >= FFT_FRAME { break }
			self.fft_buf[i].re = *f * hamming(FFT_FRAME, i);
		}

		// Fft::process_*() mutates its first arg in place	
	    // After this, fft_buf holds the complex magnitude/phase spectrum
		self.fft.process_with_scratch(&mut self.fft_buf, &mut self.fft_tmp);

		// Compute the normalized power spectrum
		let mut pspec = vec![0.0f32];
		for (pow, mag) in pspec.iter_mut().zip(&self.fft_buf) {
			*pow = (*mag * mag.conj()).re / FFT_FRAME as f32;
		}
		pspec
	}
}

fn hamming(n: usize, i: usize) -> f32 {
	const ALPHA: f32 = 0.54;
	ALPHA - (1.0 - ALPHA) * (2.0 * consts::PI * (i as f32 / (n - 1) as f32)).cos()
}